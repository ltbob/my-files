# Starship init on fish

starship init fish | source

#Any alias that I add will go here
#alias' are substitues to save time on typing.

alias battery="upower -i /org/freedesktop/UPower/devices/battery_BAT0"
#alias ls='exa -a --color=always --group-directories-first' # my preferred listing
alias ls='lsd -a --color=always --group-dirs first'
alias grep='ripgrep --color=auto'
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias update='sudo pacman -Syyu'
alias cd..='cd ..'
alias cat='bat'
alias sd='shutdown now'
