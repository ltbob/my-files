# This is my greeting for everytime I start a new console



function fish_greeting
	# Greeting messages
	set powered_msgs \
		"Narnia" \
		"Middle Earth" \
		"Mythica" \
		"Asgard" \
		"Westeros" \
		"Atlantis" \
		"Wonderland" \
		"Neverland" \
		"Camelot" \
		"Hyrule" \
		"The Forgotten Realms" \
		"Pandora" \
		"The Land of Oz" \
		"Fantasia" \
		"Azeroth" \
		"The Mushroom Kingdom" \
		"Dune" \
		"The Hundred Acre Woods"\
		"The Fire Nation"\
		"Hogwarts"\
		"Tatooine"\
		"The Dreamlands"\
		"Shangri-La"\
		"Barsoom"\
		"Fablehaven"\
		"Jumanji"\
		"Eternia"\
		"Gallifrey"\
		"Duckburg"\
		"Gotham"\
		"The Deathstar"

	# Randomly pick a message
	set chosen_msg (random)"%"(count $powered_msgs)
	set chosen_msg $powered_msgs[(math $chosen_msg"+1")]

	# Output it to the console
#	figlet "FOR THE EMPIRE!" | lolcat 
	printf "Welcome to %s!\n" $chosen_msg | lolcat
#	printf (set_color F90)"Welcome to the bridge Lord Vader\nWhat is thy bidding my master?"

neofetch

end
